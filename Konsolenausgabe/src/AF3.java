
public class AF3 {

	public static void main(String[] args) {
	System.out.printf("%-10s","Fahrenheit" );
	System.out.printf("%2s","|");
	System.out.printf("%10s\n","Celsius");
	
	System.out.printf("%s","- - - - -");
	
	System.out.printf("%-10s","-20");
	System.out.printf("%2s","|");
	System.out.printf("%10s\n","-28,89");
	
	System.out.printf("%-10s","-10");
	System.out.printf("%2s","|");
	System.out.printf("%10s\n","-23.33");
	
	System.out.printf("%-10s","+0");
	System.out.printf("%2s","|");
	System.out.printf("%10s\n","-17,78");
	
	System.out.printf("%-10s","+20");
	System.out.printf("%2s","|");
	System.out.printf("%10s\n","-6,67");
	
	System.out.printf("%-10s","+30");
	System.out.printf("%2s","|");
	System.out.printf("%10s\n","-1,11");
	}

}
